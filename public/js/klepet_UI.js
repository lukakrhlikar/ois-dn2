var kanalUporabnika;
function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function htmlSpr(sporocilo){
    return $('<div></div>').html('<i style ="font-weight: bold">' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
<<<<<<< HEAD
     sporocilo = sporocilo.replace("<", "&lt ");
    sporocilo = sporocilo.replace(">", "&gt");
     sporocilo = sporocilo.replace("/", "&#47");
    
    sporocilo = sporocilo.split(":)").join("<img src= 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
    sporocilo = sporocilo.split(":(").join("<img src= 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
    sporocilo = sporocilo.split(";)").join("<img src= 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
    sporocilo = sporocilo.split("(y)").join("<img src= 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
    sporocilo = sporocilo.split(":*").join("<img src= 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
  
    
=======
    sporocilo = prepovedaneBesede(sporocilo);
>>>>>>> Naloga_2_3
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(htmlSpr(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}


var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
<<<<<<< HEAD
    $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
=======
    $('#kanal').text(rezultat.kanal);
    kanalUporabnika=rezultat.kanal;
    socket.emit('posredujKanal',kanalUporabnika);
>>>>>>> Naloga_2_5
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
 
  
  socket.on('kanali', function(kanali) {
    $('#KANALI').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal !== '') {
        $('#KANALI').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#KANALI div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
 
  socket.on('uporabnikii', function(podatkiUporabnika){
        $('#VZDEVKI').empty();
      $('#VZDEVKI').append(htmlSpr(podatkiUporabnika.vzdevek));
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('posredujKanal',kanalUporabnika);
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function stZvezdic(n){
  var zvezdice="";
  for (var i=0; i<n;i++){
    zvezdice+="*";
  }
  return zvezdice;
}

function prepovedaneBesede(sporocilo){
  var list = izXMLvDOM("../swearWords.xml");
  var prepovedanebesede = list.getElementsByTagName("word");
  var beseda = "";
  
  //for (var i=0; i<prepovedanebesede.length;i++){
    //beseda = prepovedanebesede[i].childNodes[0].nodeValue;
    //console.log(beseda);
    
    //sporocilo = sporocilo.replace(new RegExp("\\b"+beseda+"\\b"),stZvezdic(beseda.length));
    //sporocilo.split(new RegExp("\\b"+beseda+"\\b")).join(stZvezdic(beseda.length));
    //sporocilo =  sporocilo.split(new RegExp("\\b"+beseda+"\\b")).join(stZvezdic(beseda.length));
  //}
  for (var i=0; i<prepovedanebesede.length;i++){
    beseda = prepovedanebesede[i].childNodes[0].nodeValue;

    beseda = (beseda+'').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");    
    sporocilo =  sporocilo.replace(new RegExp("\\b"+beseda+"\\b", "gi"), stZvezdic(beseda.length));
  }
  
  return sporocilo;
}

function izXMLvDOM(document){
  var httpx = new XMLHttpRequest();
  httpx.open("GET",document,false);
  httpx.send();
  var xmlDoc= httpx.responseXML;
  return xmlDoc;
}
